# README for Bryan May (He/Him)

# Who I Am
I am from Central New Jersey, USA. I have degrees in Computer Egineering and Software Systems Engineering. Early in my career, I worked with the US Army as a Civilian at Fort Monmouth, NJ and Aberdeen Proving Ground, MD. I have a [patent](http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&p=1&u=%2Fnetahtml%2FPTO%2Fsearch-bool.html&r=1&f=G&l=50&co1=AND&d=PTXT&s1=%22Element-centric+communication+map%22.TI.&OS=TTL/) from my time working as a civilian with the Army.

I transitioned to the private sector in 2017 with Verizon, where I was working as product owner for the enterprise devops platform. During this time I had awesome exposure to enterprise problems including App Modernization, Cloud Migration and DevOps Transformation. I helped manage the complete rollout of GitLab including data/user migration and infrastructure deployment. I built a communication plan for 10K+ developers using the enterprise SCM tools. 

I am a student of Agile and DevOps. I have had exposure early in my career to waterfall and painful multi-year lead-ups to integration and testing events that never went to plan. These experiences ultimately drove me toward a career in the DevOps Community; and for this, I am grateful.

# What to Expect from me
## My Role
I am the leader of the Global [Engagement Management](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/engagement-mgmt/) organization within the [Professional Services](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/) department at GitLab. We are responsible for the Global services go to market motion, helping account teams and customers accelerate to value realization and de-risking their journey with GitLab. I'm grateful to work with a team of professionals who have industry expertise to ensure high quality of delivery and amazing results. 

My ultimate goal is to help our customers win in the marketplace. They can do that by accelerating their time to value with professional services without paying the opportunity costs of redirecting talent to focus on migration, transition, and implementation initiatives. My mission is to ensure that GitLab PS brings to market increasingly valuable service offerings to ensure we're able to help customers from all segments and regions. This means making sure we have the right service offerings in place with the right partnerships to sell and deliver them to our ever-growing number of customers. 

## How I work
I tend to start around 8am ET and officially finish aroud 5pm ET on most working days. I sometimes hop back on after 7:30pm to review west coast team members pings/questions, etc but I'm striving to not do this as much as I can to lead by example of setting work/life boundaries. I check email periodically usually once in the morning and once in the afternoon. I am most reachable with the shortest response time on Slack. 

On a daily basis, I use the following workflow:
1. review open slack mentions and posts on certain channels (continously throughout the day)
1. review gitlab todos (usually a few times per day in between meetings)
1. review email (once or twice per day)
1. review personal backlog - I keep this in an offline note. These usually have links to docs or issues, but its my way of keeping track of the in flight items that might exist across technology platforms (g suite, gitlab, email, slack) 

I'm a big fan of the documentation-first collaboration technique that GitLab has helped coin. And I really hold transparency as a core personal value, so I'm happy to collaborate on an issue, MR, or google doc (taking notes in real time).  I think during initial brainstorming sessions, docs are better than issues as many people can collaborate in real time. Once we have some actions and an idea formulated, I apprecaite using gitlab issues and MRs to ensure we can track them to completion and effectively collaborate with each other given our geographically distributed nature. 

## Working with me
- If you need my immediate attention on something, _at mention_ me in the most relevant slack channel (avoid DMs and group DMs when possible)
- If you haven't gotten a response from me in over a day on a GitLab issue, google doc mention, etc - _at mention_ me somewhere in slack.
- Please try to [refrain from using slack DMs](https://about.gitlab.com/handbook/communication/#do-not-use-group-direct-messages) unless there is something sensitive that shouldn't be known by a large crowd. I prefer to discuss in open channels to enable others to have awareness of what is happening around them. 
- When brainstorming ideas, building diagrams, creating proposals, etc. please link to the notes doc, issue, or slack thread where the topic first came up. It i important for me to have cross-linked information so I can be [efficienct](https://about.gitlab.com/handbook/values/#efficiency) with my time and yours. 
- I value continuous improvement, so if there is something that I can be doing better to work with you, please [let me know](https://docs.google.com/forms/d/e/1FAIpQLSf6ALw7ZHjJy7JjDXX2Cuc18h7leWpbBrPYmJoCd9PUBqwdYA/viewform?usp=sf_link). I love hearing constructive criticism.
- I am an [analyst/architect personality type](https://www.16personalities.com/intjs-at-work), which can be useful to know when working with me. 
- Want to get to know me better? I would love to have a coffee chat! Just slam it on my calendar within my working hours and I'll be there.


## Reading List
_This list of books has helped me arrive at the role that I'm in today. Its a mix of devops, product management and Sales Leadership references from thought leaders in the industry._

- [The Qualified Sales Leader](https://www.amazon.com/s?k=the+sales+qualified+leader&hvadid=601145572934&hvdev=c&hvlocphy=9003689&hvnetw=g&hvqmt=e&hvrand=11227485298707000402&hvtargid=kwd-1662764977608&hydadcr=7662_9903423&tag=googhydr-20&ref=pd_sl_37ipx6exir_e) by John McMahon
- [Sooner Safer Happier](https://www.amazon.com/Sooner-Safer-Happier-Patterns-Antipatterns/dp/1942788916) by Jon Smart
- [Never Split the Difference: Negotiate Like your life depended on it](https://www.amazon.com/Never-Split-Difference-Negotiating-Depended/dp/0062407805) by Chris Voss
- [Lean Product Playbook](https://www.amazon.com/Lean-Product-Playbook-Innovate-Products/dp/1118960874/ref=asc_df_1118960874/?tag=hyprod-20&linkCode=df0&hvadid=312111907622&hvpos=1o1&hvnetw=g&hvrand=9352777450239898178&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=9006117&hvtargid=pla-434197569733&psc=1) by Dan Olsen
- [The Unicorn Project](https://www.amazon.com/Unicorn-Project-Developers-Disruption-Thriving-ebook/dp/B07QT9QR41) by Gene Kim
- [The Phoenix Project](https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business-ebook/dp/B078Y98RG8/ref=sr_1_3?crid=3A5FWTSIGWRTT&dchild=1&keywords=the+phoenix+project&qid=1593607206&s=digital-text&sprefix=the+phoeni%2Cdigital-text%2C146&sr=1-3) by Gene Kim
- [Investments Unilimted](https://www.amazon.com/Investments-Unlimited-Security-Compliance-Thriving/dp/B0B5LV5F42/ref=sr_1_1?crid=2HV0D6NRL0KLI&keywords=investments+unlimited&qid=1683746017&s=books&sprefix=investments+unlimited%2Cstripbooks%2C70&sr=1-1)
- [Inspired: How to create Tech products customers love](https://www.amazon.com/Inspired-Create-Products-Customers-Second/dp/B07BDQVC45/ref=sr_1_1?crid=JQ3ADLLC8OFA&keywords=inspire+marty+cagan&qid=1562347820&s=books&sprefix=inspired+mar%2Cstripbooks%2C272&sr=1-1) by Marty Cagen
- [Sprint: Solve big problems and test new ideas in just five days](https://www.amazon.com/Sprint-Solve-Problems-Test-Ideas/dp/B019R2DQIY/ref=pd_sbs_129_3/143-0515538-6527614?_encoding=UTF8&pd_rd_i=B019R2DQIY&pd_rd_r=936a63fe-9f4a-11e9-92c9-7b5b72fd778d&pd_rd_w=bssUA&pd_rd_wg=n37ki&pf_rd_p=588939de-d3f8-42f1-a3d8-d556eae5797d&pf_rd_r=RVAMV5CAA6FFQEZXMF7R&psc=1&refRID=RVAMV5CAA6FFQEZXMF7R) by Jake Knapp (I haven't actually read this, but the concepts are summarized here: https://www.gv.com/sprint/)
- [Measure what Matters - OKRs](https://www.amazon.com/Measure-What-Matters/dp/B07BMJ4L1S/ref=sr_1_1?keywords=okr&qid=1562348030&s=audible&sr=1-1) by John Doerr
- [Mindset: The new Psychology of Success](https://www.amazon.com/Mindset-Psychology-Carol-S-Dweck/dp/0345472322) by Carol Dweck
- [Project to Product](https://www.amazon.com/Project-Product-Survive-Disruption-Framework/dp/B07KCLW84N/ref=sr_1_2?keywords=project+to+product&qid=1562353329&s=books&sr=1-2) by Mik Kersten
- [Making Work Visible: Exposing Time Theft to Optimize Work & Flow](https://www.amazon.com/Making-Work-Visible-Exposing-Optimize/dp/1942788150) by Dominica Degrandis
- [Mindset: The New Psychology of Success](https://www.amazon.com/Mindset-Carol-S-Dweck-audiobook/dp/B07N48NM33/ref=sr_1_1?dib=eyJ2IjoiMSJ9.Nu_2yJCG8MYrnDRLvJ54JMBPp0CftEqSAxbmGBfDW1ymrmTuQAa7BnBdwXVj6fpe3Vywf2J-MImpzIBjbC_N4SOZVu6ElNdy7pq9P9150zhr70yd2XGcX1Sy_56LCC2lL8seBzMq3cA8ALmndxpEHARA25-exu2CXu5lOpocw83xuQKraqy9RSB6VcCwHjU-1i_YtpcYUqjYo4ARjWpTdTd5kVOynOXQMGrn67Un5zY.27TGz3DhKADmuUo2sCowzAprYsKmNqFV4WCDiKWggYM&dib_tag=se&hvadid=676976289016&hvdev=c&hvlocphy=9003689&hvnetw=g&hvqmt=e&hvrand=15366895045736974561&hvtargid=kwd-432856999365&hydadcr=22163_13517541&keywords=mindset+carol+dweck+book&qid=1711040339&sr=8-1) by Carol Dweck
- https://www.producttalk.org/ is a great blog by Teresa Torres about product discovery.
- [Opportunity Roadmaps](https://medium.com/@jefago/how-do-you-create-a-product-roadmap-d41a42b4a543) is a blog post about how to shift the thinking about roadmaps from features to opportunities
- [Anything by Malcolm Gladwell](https://www.amazon.com/Malcolm-Gladwell/e/B000APOE98)
- [The DevOps Handbook](https://www.amazon.com/DevOps-Handbook-World-Class-Reliability-Organizations-ebook/dp/B01M9ASFQ3/ref=sr_1_3?dchild=1&keywords=devops+handbook&qid=1593607226&s=digital-text&sr=1-3) by Gene Kim and Jez Humble
